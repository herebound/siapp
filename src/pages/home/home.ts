import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingPage } from '../loading/loading';
import { OSCService } from '../../services/osc.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  backimg: string;

  constructor(public navCtrl: NavController, private oscService: OSCService) {
  }

  ionViewDidLoad() {
    this.oscService.openPort();
  }

  startSystem(){
    this.oscService.sendStartOSC();
    this.navCtrl.push(LoadingPage);
  }



}
