import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PlaygroundPage } from '../playground/playground';

@IonicPage()
@Component({
  selector: 'page-loading',
  templateUrl: 'loading.html',
})
export class LoadingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.goToPlayground();
  }

  goToPlayground() {
    this.createTimeout(40000).then(()=>{this.navCtrl.setRoot(PlaygroundPage);
    })
  }

  createTimeout(timeout) {
       return new Promise((resolve, reject) => {
           setTimeout(() => resolve(null),timeout)
       })
   }


}
