import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { OSCService } from '../../services/osc.service';

@IonicPage()
@Component({
  selector: 'page-playground',
  templateUrl: 'playground.html',
})

//TODO: position coordinates test
//TODO: code sugarn
//TODO: object multiple sending possible? prevent?


export class PlaygroundPage {

  //get the id of the destroyed object from oscService
  private idSubscribtion: Subscription;

  //constructor, declares the services used
  constructor(public navCtrl: NavController, public navParams: NavParams, private oscService: OSCService) {
  }

  ionViewDidLoad() {
    // this.oscService.openPort();
  }

  ngOnInit() {
   let startX: number;
   let startY: number;
   let endX: number;
   let endY: number;
   let vergleichX: number;
   let draggedEl: HTMLDivElement;
   let dragging: boolean = false;
   let startpositions: {x: number, y: number}[];

   const elements: NodeListOf<HTMLDivElement> = <NodeListOf<HTMLDivElement>>document.querySelectorAll('.playbutton');
   const wrapper: HTMLDivElement = <HTMLDivElement>document.querySelector('.wrapper');
  //  const wrapperWidth = +window.getComputedStyle(wrapper).width.split('px')[0];
  //  const wrapperHeight = +window.getComputedStyle(wrapper).height.split('px')[0];

   startpositions = Array.from(elements).map((element, index)=> ({x: element.clientLeft, y: element.clientTop}));

   //moves the object back to its startposition when it got destroyed
   this.idSubscribtion = this.oscService.onId$.subscribe(id => {
     elements.item(id).style.transform = `translate(${startpositions[id].x}px, ${startpositions[id].y}px)`;
     elements.item(id).dataset.locked = "false";
     elements.item(id).classList.add("animating");
     setTimeout(() => elements.item(id).classList.remove("animating"), 300);
   });

   //handle the movement of the objects
   const handleMovement = (ev: TouchEvent) => {
     if(!draggedEl || draggedEl.dataset.locked=="true") return;
     if(!dragging) return;
     if (ev.touches.length === 0) return;
     const x = -(startX - ev.touches.item(0).clientX);
     const y = -(startY - ev.touches.item(0).clientY);
     vergleichX = ev.touches.item(0).clientX;
    //  endX = x/ -(window.innerWidth / 100) / 100;
    //  endY = y/ (window.innerHeight / 100) / 100;
     draggedEl.style.transform = `translate(${x}px, ${y}px)`;
   };

   //starting function for the movement of the objects
   const startMovement = (ev: TouchEvent, el: HTMLDivElement) => {
     if (ev.touches.length === 0) return;
     startX = ev.touches.item(0).clientX;
     startY = ev.touches.item(0).clientY;
     draggedEl = el;
     dragging = true;
   };

   //locks the object to the position it was layed down
   const endMovement = (ev: TouchEvent) => {
     if (!draggedEl) return;
     if(endX < -(draggedEl.clientWidth)){
       draggedEl.dataset.locked = "true";
     }

     //get the id from the draggedEl + create tmp data
     const id = +draggedEl.dataset.id;
     const tmp = 0;

     let offSet = wrapper.offsetWidth;

     if (vergleichX < offSet){

       //normalizes the position of the object
       //console.log(x / (window.innerWidth / 100) / 100, y / (window.innerHeight / 100) / 100);
      //  const x = endX; //TODO:values are wrong, calculate again!
      //  const y = endY;
      const x = getOffset(draggedEl).left / wrapper.clientWidth;
      const y = getOffset(draggedEl).top / wrapper.clientHeight;
      //alert(x/ (wrapper.clientWidth / 100) / 100);
      //alert(y/ (window.innerHeight / 100) / 100);


       //send the id, tmp data and normalized position of the object to the CoreLogic
       this.oscService.sendOSC(id, tmp, x, y);
       dragging = false;
       draggedEl.dataset.locked = "true";
     } else if (vergleichX >= offSet) {
    //    //resetElement to startpositon
    // alert("cant be set here");
       elements.item(id).style.transform = `translate(${startpositions[id].x}px, ${startpositions[id].y}px)`;
       elements.item(id).dataset.locked = "false";
       elements.item(id).classList.add("animating");
       setTimeout(() => elements.item(id).classList.remove("animating"), 300);
     }
   };

   //eventlistener
   document.addEventListener('touchend', endMovement);
   document.addEventListener('touchcancel', endMovement);
   document.addEventListener('touchmove', handleMovement);

   //function of the movement
   for(let i=0;i<elements.length; i++){
     elements[i].dataset.locked = "false";
      elements[i].addEventListener('touchstart', (ev: TouchEvent) => {
        startMovement(ev, elements[i]);
      });
   }
   function getOffset(el) {
  el = el.getBoundingClientRect();
  return {
    left: el.left + el.width/2 + window.scrollX,
    top: el.top + + el.height/2 + window.scrollY
  }
}
}

  ngOnDestroy() {
    this.idSubscribtion.unsubscribe();
    this.oscService.closePort();
  }
}
