import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable'
import * as osc from 'osc/dist/osc-chromeapp';

@Injectable()
export class OSCService {
  private udpPort;
  private ready: boolean;
  private _onId$ = new Subject<number>();

  onId$: Observable<number> = this._onId$.asObservable();

  constructor() {
    // Create an osc.js UDP Port listening on port 9002
    this.udpPort = new osc.UDPPort({
        localAddress: "192.168.188.33",
        // localAddress: "192.168.1.11", //testing
        localPort: 9002, //listen to
        remoteAddress: "192.168.188.33",
        remotePort: 9006, //send to
        metadata: true
    });

    // Open the socket
    this.udpPort.open();

    // Set flag when udpPort is ready
    this.udpPort.on("ready", () => {
      this.ready = true;
    });

    //get error on screen
    this.udpPort.on("error", (error)=> {
      this.udpPort.close();
      this.udpPort.open();
      alert(`error occured: ${JSON.stringify(error)}`);
    });

    // fires on successfully received message and pushes the id into observable
    this.udpPort.on("message", (msg) => {
      // alert(JSON.stringify(msg));
      this._onId$.next(msg.args[0].value);
    });
  }

  public sendStartOSC(){
    if (this.ready){
      console.log('sending!');
      this.udpPort.send({
          address: "/tablet/start",
          args: [
              {
                  type: "i",
                  value: 1
              }
          ]
      // }, "192.168.188.25", 9006); //testing localhost (always changing)
    }, "192.168.188.34", 9006); //production tracking
    }
  }

  public sendOSC(id, tmp, x, y){
    // When the port is ready, send an OSC message to, say, SuperCollider
    if (this.ready){
      console.log('sending!');
      // alert("sending");
      this.udpPort.send({
          address: "/tablet",
          args: [
              {
                  type: "i",
                  value: id
              },
              {
                  type: "i",
                  value: tmp
              },
              {
                  type: "f",
                  value: x
              },
              {
                  type: "f",
                  value: y
              }
          ]
      // }, "192.168.188.25", 9006); //testing localhost
    }, "192.168.188.34", 9006); //production tracking
    }
  }

  //deprecated - solved problem whith first calling this.udpPort.open(), then ready
  public openPort(){
    this.udpPort.open();
    // alert("ready on ngInit");
  }

  public closePort(){
    this.udpPort.close();
  }
}
