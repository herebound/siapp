import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);

// Create an osc.js UDP Port listening on port 57121.
// const udpPort = new osc.UDPPort({
//     localAddress: "0.0.0.0",
//     localPort: 57121,
//     metadata: true
// });

// // Listen for incoming OSC bundles.
// udpPort.on("bundle", (oscBundle, timeTag, info) => {
//     // console.log("An OSC bundle just arrived for time tag", timeTag, ":", oscBundle);
//     // console.log("Remote info is: ", info);
// });

// // Set flag when udpPort is ready
// udpPort.on("ready", () => {
//   alert('rady');
//     this.udpPort.send({
//         address: "/s_new",
//         args: [
//             {
//                 type: "s",
//                 value: "default"
//             },
//             {
//                 type: "i",
//                 value: 100
//             }
//         ]
//     }, "192.168.1.10", 57110);
// });
//
// // Open the socket.
// udpPort.open();
