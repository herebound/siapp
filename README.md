# SIApp - Setup #
The following steps describe whats necessary to get the application running.

### How do I get started? ###

You need to install:
 - ionic angular @3.3.0
 - ionic-cli @1.3.0
Go to https://ionicframework.com/docs/intro/installation/ to find out how.

### npm install ###
Make sure osc.js and cordova-udp-plugin are installed!
In your console, navigate to your projectfolder and type the following commands:
 -$ npm install osc.js --save--dev 
 -$ ionic cordova install plugin cordova-udp-plugin --save--dev

You have to modify your osc.js Plugin!
There seems to be a bug in osc.js so you have to fix it manually after installing it.
go to /node_modules/osc/dist/osc_chromeapp.js and search for "../" and replace it with "./".
This will make osc.js run properly.

When done, insert the Following code into your config.xml and AndroidManifest.xml
### config.xml: ###
<preference name="Fullscreen" value="true" />

### AndroidManifest.xml ###
android:theme="@android:style/Theme.Black.NoTitleBar.Fullscreen"


### How do I deploy? ###
Just connect your Android-device and type the following command in your Terminal while in the SIApp folder.
 -$ ionic cordova run android

for development with livereload type:
 -$ ionic cordova run android -lc

This also works with a virtual device - make sure to have Android Studio installed.
There you also can set up a virtual device. This only works for Testing!
It's not Possible to send OSC via the virtual Device to the Show Interactive CoreLogic!


### Please note! ###
The application is meant to only run in the Show Interactive environment which includes
a fixed IP-address for the Tablet, on which the application is running.
The current IP is 192.168.188.33.

If you want to run the application with another IP you have to adjust the IP
in the osc.service.ts. You can find it under src/services.
Find out your current IP and apply it before deploying.
